# Vagrant Symfony Demo
------------

Description
------------
Le but de ce projet est de déployer une machine virtuelle permettant de tester le framework symfony 3.

Vagrant et Ansible seront utilisés afin de déployer cet environnement.

La machine virtuelle est une debian 9(stretch), les packages nécessaires à l'execution de la demo symfony sont installés via ansible depuis les dépots officiels debian.

Afin d'utiliser une base postgres dans la demo Ansible se charge de plusieurs actions :
- Créér une base de donnée postgres symfony 
- Modifier le parameters.yml.dist avec l'URL de la base
- Créér et charger les données de la démo via doctrine 

Packages principaux :
- Nginx 1.10
- PostgreSQL 9.6
- php-fpm 7.0
- php-codesniffer 2.7
- phpunit 5.4.6
- phing 2.14
- ant 1.9.9


Pré-requis
------------
* Installer [VirtualBox](https://www.virtualbox.org/) et les VboxGuestAdditions
* Installer Vagrant en suivant les instructions : [Getting Started document](https://www.vagrantup.com/docs/getting-started/)
* Installer les plugins vagrant vagrant-vbguest et vagrant-hostsupdater ( ```vagrant plugin install vagrant-hostsupdater vagrant-vbguest ```)
* Le code source de la démo (https://github.com/symfony/symfony-demo) doit se trouver dans un repertoire relatif à ce projet : ../symfony_demo/


Utilisation
------------
* Cloner ce repository ( ```git clone https://gitlab.com/Koji/vagrant_symfony_demo.git ```)
* Installer une version demo de symfony dans ../symfony_demo
* Executer ```vagrant up```
* Vous pouvez accèder depuis un navigateur à l'adresse : http://symfony-demo.local/
* Vous pouvez executer des tests unitaire ou controler le code via les binaires installés en vous connectant sur le serveur virtuel via la commande  ```vagrant ssh```

Exemple de tests unitaire:
```bash
vagrant@symfony-demo:/www$ phpunit /www/tests/
PHPUnit 5.4.6 by Sebastian Bergmann and contributors.

.................................................                 49 / 49 (100%)

Time: 27.26 seconds, Memory: 56.00MB

OK (49 tests, 88 assertions)
```

Exemple de controle du code avec php-codesniffer:
```bash
vagrant@symfony-demo:/www$ phpcs /www/app/

FILE: /www/app/AppCache.php
----------------------------------------------------------------------
FOUND 2 ERRORS AFFECTING 2 LINES
----------------------------------------------------------------------
  2 | ERROR | You must use "/**" style comments for a file comment
 14 | ERROR | Missing class doc comment
----------------------------------------------------------------------


FILE: /www/app/AppKernel.php
----------------------------------------------------------------------
FOUND 7 ERRORS AND 3 WARNINGS AFFECTING 10 LINES
----------------------------------------------------------------------
  2 | ERROR   | You must use "/**" style comments for a file comment
 15 | ERROR   | Missing class doc comment
 17 | ERROR   | Missing function doc comment
 33 | WARNING | Line exceeds 85 characters; contains 146 characters
 44 | WARNING | Line exceeds 85 characters; contains 89 characters
 48 | WARNING | Line exceeds 85 characters; contains 87 characters
 61 | ERROR   | Missing function doc comment
 66 | ERROR   | Missing function doc comment
 71 | ERROR   | Missing function doc comment
 76 | ERROR   | Missing function doc comment
----------------------------------------------------------------------

Time: 102ms; Memory: 4Mb
```

Pour réinitialiser l'environnement : ```vagrant destroy``` puis ```vagrant up```

Les tests ont été effectués sur deux environnements différents :

| Logiciel      |     Environnement 1     |   Environnement 2     |
| ------------- | ----------------------  | ---------      |
| Host OS       |   Ubuntu 16.04          |   Windows 7 + Cygwin   |
| VirtualBox    |   5.1.28     |     5.1.26  |
| Vagrant       |   1.9.8    |   1.9.5     |
| symfony-demo  |  [symfony-demo-1.0.5.zip](https://github.com/symfony/symfony-demo/archive/v1.0.5.zip)    |   [symfony-demo-0.9.7.zip](https://github.com/symfony/symfony-demo/archive/v0.9.7.zip)     |

